# CI feedback from gitlab
_This only works when built on gitlab 8)_
[![pipeline status](https://gitlab.com/jwermuth/gradle-plugin-audit-with-git/badges/master/pipeline.svg)](https://gitlab.com/jwermuth/gradle-plugin-audit-with-git/commits/master)

# Whats this plugin good for
It will let you track (audit) information about important gradle tasks in git.

# How does it work
Heres an example from the tests
```groovy
    def "The user can activate git-auditing on a user defined task"() {
        given:
        buildFile << """
            plugins {
                id 'dk.lundogbendsen.gradle.plugins.auditwithgit'
            }
            
            task auditMe() {}
            
            auditwithgit.audit(auditMe)
        """
        when:
        def build = buildWithTasks('tasks')

        then:
        build.task(':tasks').outcome == TaskOutcome.SUCCESS
    }
```
where the user lets the plugin audit a task 'auditMe'. When 'auditMe' is executed, the plugin makes sure that:
1. everyhing is commited and in sync with current branch 
1. a commit is performed before executing 'auditMe'. The commit contains information about who _attempted to do_ what when
1. a commit is performed after executing 'auditMe'. The commit contains information about who _has done_ what when

If one of the steps fails, execution stops. If it is 'auditMe' that fails, there will be a trace in git telling that 
a named git user tried to perform 'auditMe' and the fact that there is no following "success" commit tells that 
execution of 'auditMe' failed.

# Messages in git
Its expressive and fun to use Emojis, so I do that. Check it out
https://gist.github.com/parmentf/035de27d6ed1dce0b36a
Warning: It seems its not possible to click a commit in Gitlab UI commit listing of the message of the commit is ONLY an
emoji, so you have to add some text in addition to the emoji if you want it to work

# Development tips

If you want to link to libraries locally instead of using the deployed jars

settings.gradle
```
include 'gradle-test-spock-helpers'
project(':gradle-test-spock-helpers').projectDir = file("../danskespil/gradle-plugin-terraform/gradle-test-spock-helpers")
include 'gradle-task-helpers'
project(':gradle-task-helpers').projectDir = file("../danskespil/gradle-plugin-terraform/gradle-task-helpers")
```
build.gradle
```
compile project(':gradle-task-helpers')
testCompile project(':gradle-test-spock-helpers')
```

## Commands

* Release, following ajoberstar plugin https://github.com/ajoberstar/gradle-git/wiki/Release%20Plugins
  * ./gradlew clean release -Prelease.scope=major_minor_OR_patch -Prelease.stage=final_OR_rc_OR_milestone_OR_dev
  * e.g. ./gradlew clean release -Prelease.scope=patch -Prelease.stage=final
  * ./gradlew clean release # snapshot version
  * ./gradlew clean release -Prelease.scope=patch -Prelease.stage=dev # e.g. fiddling with readme
  
# Deployment
* Deployment to bintray.
  * Manually
    * Provide credentials. I have added mine to ~/.gradle/gradle.properties:
      * bintrayUser=USER
      * bintrayApiKey=KEY
    * ./gradlew clean build generatePomFileForMyPublicationPublication bintrayUpload
  * Let CI do it for you
    * On branc BRANCH
      * Write whatever code you want to release
      * ./gradlew clean check // test it
      * ./gradlew release -Prelease.scope=patch -Prelease.stage=final // use the release task to tag it in git
    * git checkout publish // 
    * git merge BRANCH
    * git push
    * git checkout BRANCH // to back to where you came from

Releases to bintray are not automatically published, so you have to go to bintray and click your way to the last part.
