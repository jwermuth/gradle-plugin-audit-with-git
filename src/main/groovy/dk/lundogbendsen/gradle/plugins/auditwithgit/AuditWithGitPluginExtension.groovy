package dk.lundogbendsen.gradle.plugins.auditwithgit

import dk.lundogbendsen.gradle.plugins.auditwithgit.commands.GitCommand
import org.gradle.api.Project
import org.gradle.api.Task

import java.text.SimpleDateFormat

class AuditWithGitPluginExtension {
    static private Project project

    AuditWithGitPluginExtension(Project project) {
        this.project = project
    }

    static void audit(Task taskToAudit) {
        Task nothingToCommit = project.tasks.create("${taskToAudit.name}NothingToCommit", GitCommand) {
            commandsFlagsAndArgs = 'diff-index --quiet HEAD --'
        }
        taskToAudit.dependsOn nothingToCommit

        def annotation = "${taskToAudit.name}-${uniqueStringAcrossMachines()}"
        def comment = "Preparing to execute strictAudit task [${taskToAudit.name}] in [${project.projectDir}]"
        Task tagBefore = project.tasks.create("${taskToAudit.name}TagBefore", GitCommand) {
            commandsFlagsAndArgs = "tag -a ${annotation} " + "-m \"${comment}\""
        }
        taskToAudit.dependsOn tagBefore

        Task remoteUpdate = project.tasks.create("${taskToAudit.name}RemoteUpdate", GitCommand) {
            commandsFlagsAndArgs = 'remote update'
        }
        taskToAudit.dependsOn remoteUpdate

        Task saveMerge = project.tasks.create("${taskToAudit.name}SaveMerge", GitCommand) {
            commandsFlagsAndArgs = 'merge --ff-only'
        }
        taskToAudit.dependsOn saveMerge

        // order of dependencies
        saveMerge.dependsOn remoteUpdate
        nothingToCommit.dependsOn saveMerge
        tagBefore.dependsOn nothingToCommit
    }

    static private String uniqueStringAcrossMachines() {
        return "localtime_${new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss_SSS").format(new Date())}_uuid_${java.util.UUID.randomUUID().toString()}"
    }
}
