package dk.lundogbendsen.gradle.plugins.auditwithgit.commands

import dk.danskespil.gradle.task.helpers.CommandLine
import dk.danskespil.gradle.task.helpers.GradleServiceExecuteOnOS
import dk.danskespil.gradle.task.helpers.GradleServiceExecuteOnOSFactory
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import org.gradle.api.tasks.TaskAction
import org.gradle.process.ExecSpec

class GitCommand extends DefaultTask {
    @Internal
    CommandLine commandLine = new CommandLine()
    @Internal
    GradleServiceExecuteOnOS executor = GradleServiceExecuteOnOSFactory.instance.createService(project)
    @Input
    String commandsFlagsAndArgs = ""

    @TaskAction
    action() {
        commandLine.addToEnd('git')
        commandLine.addToEnd(commandsFlagsAndArgs)
        executor.executeExecSpec(this, { ExecSpec e ->
            e.commandLine this.commandLine
        })
    }
}
