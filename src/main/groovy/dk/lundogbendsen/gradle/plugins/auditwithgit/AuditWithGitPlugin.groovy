package dk.lundogbendsen.gradle.plugins.auditwithgit

import org.gradle.api.Plugin
import org.gradle.api.Project

class AuditWithGitPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.extensions.create("auditwithgit", AuditWithGitPluginExtension, project)
    }
}
