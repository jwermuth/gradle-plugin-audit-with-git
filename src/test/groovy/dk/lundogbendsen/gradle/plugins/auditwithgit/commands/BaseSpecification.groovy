package dk.lundogbendsen.gradle.plugins.auditwithgit.commands

import dk.danskespil.gradle.task.helpers.GradleServiceExecuteOnOSFactory
import dk.danskespil.gradle.test.spock.helpers.TemporaryFolderSpecification

class BaseSpecification extends TemporaryFolderSpecification {
    def setup() {
        setupTestStubsByAddingMarkerFilesToAConventionNamedDirectory()
    }

    void setupTestStubsByAddingMarkerFilesToAConventionNamedDirectory() {
        GradleServiceExecuteOnOSFactory.instance.enableStub()
    }
}
