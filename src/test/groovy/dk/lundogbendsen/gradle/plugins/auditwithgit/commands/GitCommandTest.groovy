package dk.lundogbendsen.gradle.plugins.auditwithgit.commands

import org.gradle.testkit.runner.TaskOutcome

class GitCommandTest extends BaseSpecification {
    def "can create task"() {
        given:
        buildFile << """
         plugins {
                id 'dk.lundogbendsen.gradle.plugins.auditwithgit'
         }
         
         task cut(type:dk.lundogbendsen.gradle.plugins.auditwithgit.commands.GitCommand) 
"""
        when:
        def build = buildWithTasks(':cut')

        then:
        build
        build.task(':cut').outcome == TaskOutcome.SUCCESS
        build.output.contains('git')
    }

    def "Can execute a command"() {
        given:
        buildFile << """
         plugins {
                id 'dk.lundogbendsen.gradle.plugins.auditwithgit'
         }
         
         task cut(type:dk.lundogbendsen.gradle.plugins.auditwithgit.commands.GitCommand) {
            commandsFlagsAndArgs='help'
         } 
"""
        when:
        def build = buildWithTasks(':cut')

        then:
        build
        build.task(':cut').outcome == TaskOutcome.SUCCESS
        build.output.contains('git help')
    }

    def "Can execute a multipart command"() {
        given:
        buildFile << """
         plugins {
                id 'dk.lundogbendsen.gradle.plugins.auditwithgit'
         }
         
         task cut(type:dk.lundogbendsen.gradle.plugins.auditwithgit.commands.GitCommand) {
            commandsFlagsAndArgs='help commit'
         } 
"""
        when:
        def build = buildWithTasks(':cut')

        then:
        build
        build.task(':cut').outcome == TaskOutcome.SUCCESS
        build.output.contains('git help commit')
    }
}
