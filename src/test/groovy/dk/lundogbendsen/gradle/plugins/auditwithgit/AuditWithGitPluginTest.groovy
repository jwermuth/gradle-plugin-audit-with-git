package dk.lundogbendsen.gradle.plugins.auditwithgit

import dk.lundogbendsen.gradle.plugins.auditwithgit.commands.BaseSpecification
import org.gradle.testkit.runner.TaskOutcome
import spock.lang.Unroll

class AuditWithGitPluginTest extends BaseSpecification {
    def "I can apply the plugin"() {
        given:
        buildFile << """
            plugins {
                id 'dk.lundogbendsen.gradle.plugins.auditwithgit'
            }
        """

        when:
        def build = buildWithTasks('tasks')

        then:
        build
        build.task(':tasks').outcome == TaskOutcome.SUCCESS
    }

    def "The user can activate git-auditing on a user defined task"() {
        given:
        buildFile << """
            plugins {
                id 'dk.lundogbendsen.gradle.plugins.auditwithgit'
            }
            
            task auditMe() {}
            
            auditwithgit.audit(auditMe)
        """
        when:
        def build = buildWithTasks('tasks')

        then:
        build.task(':tasks').outcome == TaskOutcome.SUCCESS
    }

    def "it will be checked that everything is committed before the audited task is executed"() {
        given:
        buildFile << """
            plugins {
                id 'dk.lundogbendsen.gradle.plugins.auditwithgit'
            }
            
            task auditMe() {}
            
            auditwithgit.audit(auditMe)
        """
        when:
        def build = buildWithTasks('auditMe')

        then:
        build.task(':auditMeNothingToCommit')
        build.task(':auditMeNothingToCommit').outcome == TaskOutcome.SUCCESS
    }

    @Unroll
    def "#tasks will be executed before the task under audit"() {
        given:
        buildFile << """
            plugins {
                id 'dk.lundogbendsen.gradle.plugins.auditwithgit'
            }
            
            task auditMe() {}
            
            auditwithgit.audit(auditMe)
        """
        when:
        def build = buildWithTasks('auditMe')

        then:
        build.task(":auditMe${tasks}")
        build.task(":auditMe${tasks}").outcome == TaskOutcome.SUCCESS

        where:
        tasks << ['NothingToCommit', 'TagBefore', 'RemoteUpdate', 'SaveMerge']
    }


    // I was writing this (unfinished) test and functionality a looong time ago, when I was alone on the project..
    // Now it seems someone wants to help, so I am wrapping up and will start using feature-branches (aka behave better)
    // so we can work more people on the plugin
    //    @Unroll
    //    def "task #taskA is executed before task #taskB so that #reason"() {
    //        given:
    //        buildFile << """
    //            plugins {
    //                id 'dk.lundogbendsen.gradle.plugins.auditwithgit'
    //            }
    //
    //            task auditMe() {}
    //
    //            auditwithgit.audit(auditMe)
    //        """
    //        when:
    //        BuildResult build = buildWithTasks('auditMe','--dry-run')
    //
    //        then:
    //        taskAIsExecutedBeforeTaskB(build, ":auditMe${taskA}", ":auditMe${taskB}")
    //
    //        where:
    //        taskA       | taskB          | reason
    //        'RemoteUpdate' | 'SaveMerge' | 'latest commits from remote is up2date before we attempt a merge'
    //
    //    }

    //    void taskAIsExecutedBeforeTaskB(BuildResult build, String before, String after) {
    //        // The output from --dry-run is ordered. The execution order is not guaranteed, but its a guarantee that
    //        //   at least in this case it will be executed in this order
    //        TreeSet<OrderedTask> orderedTasks = new TreeSet<OrderedTask>()
    //        orderedTasks.add(new OrderedTask(before, build.output.indexOf(before)))
    //        orderedTasks.add(new OrderedTask(after, build.output.indexOf(after)))
    //        assert orderedTasks.
    //
    //        //assert build.output.indexOf(before) < build.output.indexOf(after)
    //    }
}
